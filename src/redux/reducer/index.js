import { combineReducers, configureStore,getDefaultMiddleware  } from "@reduxjs/toolkit"
import albums from "./albums";
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist'


const rootReducer = combineReducers({
  albums
}) // Все что ниже - это стор ;

const persistConfig = {
  key: 'root',
  storage
};
const persistedReducer = persistReducer(persistConfig, rootReducer);


const middleware = [...getDefaultMiddleware({ thunk: false, serializableCheck: false })];
const store = configureStore({
  reducer:persistedReducer,
  middleware,
  devTools: process.env.NODE_ENV !== 'production',
})

export default store;