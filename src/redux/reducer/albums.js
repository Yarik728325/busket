import { createSlice } from "@reduxjs/toolkit";


export const check = createSlice({
  name:"toolkit",
  initialState:{
    data:[
      {
        id:1,
        name:'article 1',
        label:'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        price:25
      },
      {
        id:2,
        name:'article 2',
        label:'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        price:35
      },
      {
        id:3,
        name:'article 3',
        label:'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
        price:45
      },
    ],
    basket:{
      data:[],
      summ:0,
    }
  },
  reducers:{
    addToBusket(state,{payload}){
      const { price,name } = payload;
      const { data } = state.basket;
      const index = state.basket.data?.findIndex(e=>e.name === name);
      state.basket.summ+=price;
      if(index === -1){
       data.push({
          ...payload,
          count:1
        })
      }else{
        data[index].count+=1;
      }
    },
    deleteBusket(state,{payload}){
      let { data, summ } = state.basket;
      const { count, price } = state.basket.data[payload];
      state.basket.summ = summ - count * price;
      data.splice(payload,1);
    }
  }
})

export default check.reducer;

export const { addToBusket, deleteBusket } = check.actions;
