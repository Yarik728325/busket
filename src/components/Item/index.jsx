import React from "react";
import { useDispatch } from "react-redux";
import { addToBusket, deleteBusket } from "../../redux/reducer/albums";
import './style.scss';

const Item = ({name, label, price, check,count, summ, index})=>{
  const dispatch = useDispatch();
  const sendToBusket = ()=>{
    dispatch(addToBusket({
      name,
      label,
      price
    }))
  }
  const removeBusket = ()=>{
    dispatch(deleteBusket(index))
  }
  return(
    <div className="one_item">
     {
       check?(
       <>
         <h2>{name}</h2>
        <div className="descr">{label}</div>
        <div className="one_item__wrapper">
          <div className="left">Price:</div>
          <div className="right">{price}</div>
        </div>
        <button  
          className="btn" 
          onClick={()=>{
            sendToBusket();
          }} 
        >   ADD
        </button>
       </>
       ):(
          <>
            <h2>{name}</h2>
            <div className="descr">{label}</div>
            <div className="one_item__wrapper">
              <div className="left">Price:</div>
              <div className="right">{price}</div>
            </div>
            <div className="count">{count}</div>
            <button  
                className="btn" 
                onClick={()=>{
                  removeBusket();
                }} 
              >   Delete
            </button>
          </>
       )
     }
    </div>
  )
}

export default Item;