import React from "react";
import './style.scss';
import Item from "../Item";

const Items = ({ data,check, price,summ }) => {
  return(
   <div className="items">
      {
       summ?(
         <h2 className="summa">Sum:{summ}</h2>
       ):null
     }
      <div className="items__wrapper">
      {
        data.map((e,index)=>{
          const { name, label, price, count } = e;
          return(
            <Item
              name={name}
              index={index}
              label={label}
              price={price}  
              count={count}
              check={check}
              summ={summ}
              key={(Math.random() + 1).toString(36).substring(7)}
            />
            )
          })
        }
    </div>
   </div>
  )
}

export default Items;
