import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";
import Basket from "../pages/Basket";


const Routing = () =>{
  return(
    <Routes>
      <Route path="/"  element={<Home/>} />
      <Route path="/basket"  element={<Basket/>} />
      <Route path="*" element={<NotFound/>} />
    </Routes>
  )
}

export default Routing;