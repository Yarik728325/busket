import React from "react";
import { useSelector } from "react-redux";
import Items from "../../components/Items";

const Basket = () =>{
  const { basket } = useSelector(state=>state.albums);
  const { data, summ } = basket
  return(
    <>
    <Items data={data} summ={summ} />
  </>
  )

}

export default Basket;