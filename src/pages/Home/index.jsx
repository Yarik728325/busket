import React from "react";
import { useSelector } from "react-redux";
import Items from "../../components/Items";

const Home = ()=>{
  
  const { data } = useSelector(state=>state.albums);
  return (
    <>
      <Items data={data} check={true} />
    </>
  )
}

export default Home;